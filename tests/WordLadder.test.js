var a = require('../WordLadder');
var input = require('../input.json');

input.forEach(data => {
  var solution = a.findLadders(data.beginWord, data.endWord, data.wordList);
  console.log(solution);
});
