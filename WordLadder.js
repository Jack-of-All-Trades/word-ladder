/**
* @param {string} beginWord
* @param {string} endWord
* @param {string[]} wordList
* @return {string[][]}
*/
var findLadders = function (beginWord, endWord, wordList) {
  var dic = {};

  // init dictionnary
  for (var i = 0; i < wordList.length; i++) {
    dic[wordList[i]] = {
      len: Number.MAX_VALUE,
      prev: {},
    };
  }
  dic[beginWord] = {
    len: Number.MAX_VALUE,
    prev: {},
  }

  // solves the problem recursively
  // uses dic to store previous calculations
  const solve = function (word, prev, len) {
    // if the path is longer than the existing, just return
    if (dic[word].len < len) {
      return;

      // if it is equal, add the path from
      // 'prev' to 'word'
    } else if (dic[word].len === len) {
      dic[word].prev[prev] = true;
      return;
    }

    // init states
    dic[word].len = len;
    dic[word].prev = {}
    dic[word].prev[prev] = true;


    for (var i = 0; i < word.length; i++) {
      // init next char to be changed: [h]it
      var char = word.charCodeAt(i) + 1;
      // go through all letters (a-z) starting from word[i] + 1
      for (var j = 0; j < 25; j++ , char++) {
        // if next char is 'z' + 1, go back to 'a'
        if (char === 'z'.charCodeAt() + 1) char = 'a'.charCodeAt();

        // mounts the string with the char changed
        var str = word.substr(0, i) + String.fromCharCode(char) + word.substr(i + 1);

        // checks if the string is in the wordList
        if (dic[str]) {
          // word -> str;
          solve(str, word, len + 1);
        }
      }
    }
  }

  // solves the problem recursively
  solve(beginWord, beginWord, 0);

  recoverPath = function (word) {
    if (word === beginWord) {
      return [[word]];
    }
    var result = []
    if (dic[word])
      Object.keys(dic[word].prev).forEach(function (key) {
        var ok = recoverPath(key);
        var okok = ok.map(function (ans) {
          ans.push(word);
          return ans;
        });
        result = result.concat(okok);
      });
    return result;
  }
  return recoverPath(endWord);
}

module.exports = {
  findLadders
};